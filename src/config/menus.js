export default [
  {
    icon: "fas fa-chart-line",
    text: "Painel",
    path: "/app/painel",
    role: null,
  },
  {
    icon: "fas fa-user-tie",
    text: "Gestão de Usuários",
    path: "/app/usuarios",
    role: "user_management",
  },
];
