export default {
  emailRules: [
    (v) => !!v || "Informe um e-mail cadastrado.",
    (v) => /.+@.+/.test(v) || "E-mail informado não é válido.",
  ],

  passwordRules: [
    (v) => !!v || "Informe sua senha de acesso.",
    (v) => (!!v && v.length >= 8) || "Senha deve ter pelo menos 8 caracteres.",
  ],
};
