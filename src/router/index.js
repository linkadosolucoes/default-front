import Vue from "vue";
import VueRouter from "vue-router";
import store from "@/store/index";
import AuthorizationService from "@/services/AuthorizationService";

const checkAuthentication = (to, from, next) => {
  if (store.getters["getIsAuthenticated"]) {
    const meta = to.meta;
    if (meta.role !== undefined) {
      const authorized = AuthorizationService(meta.role);
      if (authorized) {
        return next();
      } else {
        return next(false);
      }
    }

    return next();
  }

  return next("/entrar");
};

const checkRedirectPage = (to, from, next) => {
  if (store.getters["getIsAuthenticated"]) {
    return next("/app/painel");
  }

  return next();
};

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    component: () =>
      import("@/components/Layouts/OutsideContextApplication.vue"),
    beforeEnter: checkRedirectPage,
    children: [
      {
        path: "/",
        component: () => import("@/views/Panel/Index.vue"),
        beforeEnter: checkAuthentication,
      },
      {
        path: "/entrar",
        component: () => import("@/views/Login/Index.vue"),
        beforeEnter: checkRedirectPage,
      },
      {
        path: "/solicitar-nova-senha",
        component: () => import("@/views/RequestNewPassword/Index.vue"),
        beforeEnter: checkRedirectPage,
      },
      {
        path: "/criar-nova-senha/:token?",
        component: () => import("@/views/CreateNewPassword/Index.vue"),
        beforeEnter: checkRedirectPage,
      },
      {
        path: "/solicitar-email-verificacao",
        component: () => import("@/views/EmailVerifyResend/Index.vue"),
        beforeEnter: checkRedirectPage,
      },
      {
        path: "/verificar-email/:token?",
        component: () => import("@/views/EmailVerify/Index.vue"),
        beforeEnter: checkRedirectPage,
      },
    ],
  },

  {
    path: "/app",
    component: () => import("@/components/EmptyParentComponent/Index.vue"),
    beforeEnter: checkAuthentication,
    children: [
      {
        path: "painel",
        component: () => import("@/views/Panel/Index.vue"),
        beforeEnter: checkAuthentication,
      },
      {
        path: "usuarios",
        component: () => import("@/components/EmptyParentComponent/Index.vue"),
        beforeEnter: checkAuthentication,
        children: [
          {
            path: "/",
            component: () => import("@/views/Users/Index.vue"),
            beforeEnter: checkAuthentication,
            meta: {
              role: "user_management",
            },
          },
          {
            path: "cadastrar",
            component: () => import("@/views/Users/Form.vue"),
            beforeEnter: checkAuthentication,
            meta: {
              role: "user_management",
            },
          },
          {
            path: "visualizar/:id",
            component: () => import("@/views/Users/Form.vue"),
            beforeEnter: checkAuthentication,
            meta: {
              role: "user_management",
            },
          },
        ],
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
