import ErrorService from "@/services/ErrorService";
import RequestService from "@/services/RequestService";

export default class RoleService extends RequestService {
  constructor(baseEndpoint) {
    super(baseEndpoint);
  }

  index = async (endpoint) => {
    try {
      const { data } = await this.get(endpoint);

      return data;
    } catch (exception) {
      throw ErrorService(exception);
    }
  };
}
