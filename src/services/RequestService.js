import api from "../plugins/axios";
import ResponseService from "./ResponseService";
import ExceptionService from "./ExceptionService";

export default class API {
  constructor(baseEndpoint) {
    this.api = api;
    this.baseEndpoint = baseEndpoint;
  }

  get = async (endpoint) => {
    try {
      const { data } = await api.get(`${this.baseEndpoint}/${endpoint}`);

      return await ResponseService(data);
    } catch (exception) {
      throw ExceptionService(exception);
    }
  };

  getById = async (endpoint, id) => {
    try {
      const { data } = await api.get(`${this.baseEndpoint}/${endpoint}/${id}`);

      return await ResponseService(data);
    } catch (exception) {
      throw ExceptionService(exception);
    }
  };

  post = async (endpoint, params) => {
    try {
      const { data } = await api.post(
        `${this.baseEndpoint}/${endpoint}`,
        params
      );

      return await ResponseService(data);
    } catch (exception) {
      throw ExceptionService(exception);
    }
  };

  put = async (endpoint, params) => {
    try {
      const { data } = await api.put(
        `${this.baseEndpoint}/${endpoint}/${params.id}`,
        params
      );

      return await ResponseService(data);
    } catch (exception) {
      throw ExceptionService(exception);
    }
  };

  delete = async (endpoint) => {
    try {
      const { data } = await api.delete(`${this.baseEndpoint}/${endpoint}`);

      return await ResponseService(data);
    } catch (exception) {
      throw ExceptionService(exception);
    }
  };

  postOrPut = async (endpoint, params) => {
    const method = params.id ? `put` : `post`;
    try {
      const { data } = await api[method](
        `${this.baseEndpoint}/${endpoint}`,
        params
      );

      return await ResponseService(data);
    } catch (exception) {
      throw ExceptionService(exception);
    }
  };
}
