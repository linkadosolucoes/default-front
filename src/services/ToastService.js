import store from "@/store/index.js";

export default (message, type) => {
  store.dispatch("toast", {
    show: true,
    color: type,
    message: message,
  });
};
